import { useState, useEffect } from "react";
import Player from "./components/Player";

function App() {
  const [songs] = useState([
    {
      title: " Kacha Badam",
      artist: "Bhuban Badyakar",
      img_src: "./images/Kacha Badam.jfif",
      src: "./music/kacha-badam.mpeg",
    },
    {
      title: "Shayad",
      artist: "Arijit Singh",
      img_src: "./images/sayad1.jfif",
      src: "./music/sayad.mp3",
    },
    {
      title: "Bella Ciao",
      artist: "Money Hiest",
      img_src: "./images/bella ciao.jfif",
      src: "./music/bella-ciao.mp3",
    },
    {
      title: "Raabta",
      artist: "Arijit Singh",
      img_src: "./images/rabta.jfif",
      src: "./music/Raabta.mp3",
    },
    {
      title: "Titaliya",
      artist: "Hardy Sandhu",
      img_src: "./images/titaliya.jpg",
      src: "./music/Titliaan Warga.mp3",
    },
    {
      title: "Nach Meri Rani",
      artist: "Yo Yo Honey Singh",
      img_src: "./images/nach-meri-rani.jpg",
      src: "./music/Naach Meri Rani.mp3",
    },
    {
      title: "Care Ni Karda",
      artist: "Yo Yo Honey Singh",
      img_src: "./images/care-ni-karda.jpg",
      src: "./music/Care Ni Karda.mp3",
    },
    {
      title: "Burj Khalifa",
      artist: "Shashi",
      img_src: "./images/burjkalifa.jpg",
      src: "./music/BurjKhalifa.mp3",
    },
    {
      title: "Tango Del Fuego",
      artist: "Parov Stelar",
      img_src: "./images/ParovStelarGeorgiaGibbs-TangoDelFuego.jpg",
      src: "./music/ParovStelarGeorgiaGibbs-TangoDelFuego.mp3",
    },
    {
      title: "Take You Dancing",
      artist: "Jason Derulo",
      img_src: "./images/JasonDerulo-TakeYouDancing.jpg",
      src: "./music/JasonDerulo-TakeYouDancing.mp3",
    },
    {
      title: "Daisy",
      artist: "Ashnikko",
      img_src: "./images/Ashnikko-Daisy.jpg",
      src: "./music/Ashnikko-Daisy.mp3",
    },
    {
      title: "Dolly Song",
      artist: "Partz Grimbad",
      img_src: "./images/PatzGrimbard-DollySong.jpg",
      src: "./music/PatzGrimbard-DollySong.mp3",
    },
  ]);
 //create a 2 custom hooks here.......
  const [currentSongIndex, setCurrentSongIndex] = useState(0); 
  const [nextSongIndex, setNextSongIndex] = useState(0);

  useEffect(() => {
    setNextSongIndex(() => {
      if (currentSongIndex + 1 > songs.length - 1) {  //index+1 is big that means no another song after that so it return to first.
        return 0;
      } else {
        return currentSongIndex + 1; 
      }
    });
  }, [currentSongIndex, songs.length]); //state change after representing of currentsongindex.

  return (
    <div className="App">
      <Player
        currentSongIndex={currentSongIndex}
        setCurrentSongIndex={setCurrentSongIndex}
        nextSongIndex={nextSongIndex}
        songs={songs}
      />
    </div>
  );
}

export default App;
